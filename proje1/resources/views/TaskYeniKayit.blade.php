@extends('layouts.appBasit')
@section('content')

<h2> Yeni Kayıt </h2>
 
@foreach ($errors->all() as $error) 
	<p>{{ $error }}</p> 
@endforeach

@if (session('status')) 
	<p> {{ session('status') }}  </p> 
@endif
			
<form method="post"  enctype="multipart/form-data" >		
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
		
	<table style="width:40%">	
		<tbody>
			<tr>
				<td> <input type="text" name="name" value="{!! $tasks->name !!}"> </td> 		
				<td> <a href="{!! action('TaskController@TaskListesi', null) !!}" class="ghost-button"> Vazgeç </a> </td> 
				<td> <button type="submit" class="ghost-button"> Kaydet </button> </td>
			</tr>
		</tbody>	
	</table>
	
</form>
 
@endsection


