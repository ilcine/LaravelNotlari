# Translation Using the Double Underscore (__) Helper Function   

Laravel çok dil kullanımında __ çift alttire kullanımı

> _Emrullah İLÇİN_

## config file  `config/app.php`

`'fallback_locale' => 'en',`  or tr, es etc.


## use ex 1

```
<?php
return [
    'welcome' => 'Welcome to our application'
];
```

```
$locale = App::getLocale();

if (App::isLocale('en')) {
    //
}
```

## blade use

{{  __('welcome') }}

or 

@lang('welcome')

 

