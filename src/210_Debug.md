# Laravel de Debug

Hata bulma özelliği

_Emrullah İLÇİN_

## QueryLog ile sql komutunu açık olarak gösterilir.

* Controller de

```
use Illuminate\Support\Facades\DB;  // DB yi etkin yap
```

* Controller function'u içinde

Örnek 1:

```php
$tasks = DB::table("SecimTakip")->toSql();
dd($tasks);
```

Örnek 2:

```php
DB::enableQueryLog();
$user = ModelAdi::all();
$query = DB::getQueryLog();
$query = end($query);
dd($query);
```

* function run ederken gelen örnek sonuç.

```json
array:3 [▼
  "query" => "select * from `ModelAdi`"
  "bindings" => []
  "time" => 1.27
]
```
