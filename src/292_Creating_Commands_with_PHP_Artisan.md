# Create Comand file

> _Emrullah İLÇİN_

php artisan ile config dosyalarını yaratmak için command file yaratılmsı


## 1) create
php artisan make:command EmrInstall --command=emr:install

## 2) edit file
app/Console/Commands/EmrInstall.php

```
public function handle()
{
    //
    $this->info("Emr, installation starts now !");
}
```

## 3) Kernel or Providers set 

### For Kernel

add the lines `edit /app/console/Kernel.php`  

```
protected $commands = [
    'App\Console\Commands\EmrInstall'
    ];
```

### For Providers

add the lines `edit /app/Providers/AppServiceProvider.php`  

```php
    public function boot()
    {
    $this->mapCommands();
    }
    
    public function mapCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
				Console\Commands\EmrInstal::class,
            ]);
        }
    }
```

## 4) see syntax

> See syntax; `php artisan help emr:install`

## 5) Clear cache // ön belleği temizle

```php artisan cache:clear```


## 6) test

`php artisan emr:install`

// you see "Emr, installation starts now !"

## 7 ex: hande inside

// text in different colors // renkleri farklı olur
```
$this->line("text default");
$this->info("Text see green!");
$this->comment("Text see yellow");
$this->question("Text see green ?");
$this->error("Text see red !!");
```

// ask
```
$name = $this->ask('What\'s your name?');
$this->info("Adınız " . $name );
```


// if 
```
 if ($this->confirm('Is '.$name.' correct, do you wish to continue? [y|N]')) 
	{ 
	    $yesorno ="y";
	    $this->info("your choose YES: " . $yesorno );
	} 
	else
	{
	    $yesorno="n";
	    $this->info("your choose NO: " . $yesorno );
	}

	$this->info("name :" . $name );
```			

// Choices

```
$default="Male";
$name = $this->choice('Your gender?', ['Male', 'Female'], $default);
```

// Function use // fonksiyon olarak kullanım

```
public function handle()
    {
	 $this->title();
	}	
protected function title()
    {	
     // etc..
    }
```

// With command line  // Komut satırında kullanım


`protected $signature = 'emr:install {user} {--gender=}';`

`php artisan emr:install emr --gender=Male`

```
$user = $this->argument('user');
$gen = $this->option('gender');
```

// full argument
$arguments = $this->arguments();

 
			 
    


