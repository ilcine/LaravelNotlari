# MODEL

Modeller Database ile ilişkiyi sağlar. 

> _Emrullah İLÇİN_

## Migration ile Database kullanımı

`Artisan` komutları kullanarak database üzerinde tablo desenleri yaratma, güncelleme gibi işlemler migration ile yapılır. database tabloları `$HOME/proje1/database/migrations/` altında tutulur.

Laravel de tablo adları küçükharf ve çoğul, model adları tekil olmalıdır. ör: tablo adı=tasks, model adı=Task

* `php artisan make:migration tasks --create=tasks`  # tasks isminde tablo yaratılır.  
> :heavy_exclamation_mark: Tablo isimleri **küçükharf** olmalı ve sonu **s** ile bitmeli, eğer bu kurala uyulmazsa Db e yazdırılmış olan tablo adı ör Task ise model dosyası içine `protected $table = 'Task';` ifadesi eklenir. 

database/migrations dizini altınta tarih formatı olan bir dosya oluşur, tablo için ihtiyaç duyulan yeni kolanlar bu dosyaya ilave edilir. 

* `php artisan migrate`  # komutu ile tablo deseni DB e yazılır.

> * `php artisan migrate:rollback --step=1` # eğer bir sorun varsa DB ye yazılan tablo geri alınır. düzenleme yapıldıktan sonra tekrar `php artisan migrate` ile DB e yazdırılır. Eğer bu ara tabloya veri girildi ise bu veriler silinir.

```php
<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class Tasks extends Migration
{
    
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
						
						// yeni tablo kolanları bu alana yazılabilir
						
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
```

## Model yaratma

* `php artisan make:model Task` # ile app/Task.php model dosyası yaratılır. Model içersine ihtiyaçlar doğrultusunda birçok tanım yapılacaktır.

```php
<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Task extends Model
{
// protected $table = 'Task';   // tablo adı tasks yerine Task yazıldı ise bu satır aktif hale getirilir  
}
```

## Test
* `php artisan tinker` komutu yaratılan model üzerinde işlem yapabilir promptu `>>>` şeklindedir, yaratılan modele  `App\Task::first();` ile bakılır. Tinker test için kullanıldı, tinker içinde model ile ilişkili birçok kullanım vardır.

```
Psy Shell v0.8.17 (PHP 7.2.2-3+ubuntu16.04.1+deb.sury.org+1 — cli) by Justin Hileman
>>> App\Task::first();
=> null
>>> exit
```

> tablo da kayıt olmadığı için null döndü, ok.


## Kolon Tipleri

|	Command	|	Description	|
|-------------------------------|--------------------------------------------------------|
|	$table->bigIncrements('id');	|	Auto-incrementing UNSIGNED BIGINT (primary key) equivalent column.	|
|	$table->bigInteger('votes');	|	BIGINT equivalent column.	|
|	$table->binary('data');	|	BLOB equivalent column.	|
|	$table->boolean('confirmed');	|	BOOLEAN equivalent column.	|
|	$table->char('name', 100);	|	CHAR equivalent column with an optional length.	|
|	$table->date('created_at');	|	DATE equivalent column.	|
|	$table->dateTime('created_at');	|	DATETIME equivalent column.	|
|	$table->dateTimeTz('created_at');	|	DATETIME (with timezone) equivalent column.	|
|	$table->decimal('amount', 8, 2);	|	DECIMAL equivalent column with a precision (total digits) and scale (decimal digits).	|
|	$table->double('amount', 8, 2);	|	DOUBLE equivalent column with a precision (total digits) and scale (decimal digits).	|
|	$table->enum('level', ['easy', 'hard']);	|	ENUM equivalent column.	|
|	$table->float('amount', 8, 2);	|	FLOAT equivalent column with a precision (total digits) and scale (decimal digits).	|
|	$table->geometry('positions');	|	GEOMETRY equivalent column.	|
|	$table->geometryCollection('positions');	|	GEOMETRYCOLLECTION equivalent column.	|
|	$table->increments('id');	|	Auto-incrementing UNSIGNED INTEGER (primary key) equivalent column.	|
|	$table->integer('votes');	|	INTEGER equivalent column.	|
|	$table->ipAddress('visitor');	|	IP address equivalent column.	|
|	$table->json('options');	|	JSON equivalent column.	|
|	$table->jsonb('options');	|	JSONB equivalent column.	|
|	$table->lineString('positions');	|	LINESTRING equivalent column.	|
|	$table->longText('description');	|	LONGTEXT equivalent column.	|
|	$table->macAddress('device');	|	MAC address equivalent column.	|
|	$table->mediumIncrements('id');	|	Auto-incrementing UNSIGNED MEDIUMINT (primary key) equivalent column.	|
|	$table->mediumInteger('votes');	|	MEDIUMINT equivalent column.	|
|	$table->mediumText('description');	|	MEDIUMTEXT equivalent column.	|
|	$table->morphs('taggable');	|	Adds taggable_id UNSIGNED INTEGER and taggable_type VARCHAR equivalent columns.	|
|	$table->multiLineString('positions');	|	MULTILINESTRING equivalent column.	|
|	$table->multiPoint('positions');	|	MULTIPOINT equivalent column.	|
|	$table->multiPolygon('positions');	|	MULTIPOLYGON equivalent column.	|
|	$table->nullableMorphs('taggable');	|	Adds nullable versions of morphs()columns.	|
|	$table->nullableTimestamps();	|	Alias of timestamps() method.	|
|	$table->point('position');	|	POINT equivalent column.	|
|	$table->polygon('positions');	|	POLYGON equivalent column.	|
|	$table->rememberToken();	|	Adds a nullable remember_tokenVARCHAR(100) equivalent column.	|
|	$table->smallIncrements('id');	|	Auto-incrementing UNSIGNED SMALLINT (primary key) equivalent column.	|
|	$table->smallInteger('votes');	|	SMALLINT equivalent column.	|
|	$table->softDeletes();	|	Adds a nullable deleted_at TIMESTAMP equivalent column for soft deletes.	|
|	$table->softDeletesTz();	|	Adds a nullable deleted_at TIMESTAMP (with timezone) equivalent column for soft deletes.	|
|	$table->string('name', 100);	|	VARCHAR equivalent column with a optional length.	|
|	$table->text('description');	|	TEXT equivalent column.	|
|	$table->time('sunrise');	|	TIME equivalent column.	|
|	$table->timeTz('sunrise');	|	TIME (with timezone) equivalent column.	|
|	$table->timestamp('added_on');	|	TIMESTAMP equivalent column.	|
|	$table->timestampTz('added_on');	|	TIMESTAMP (with timezone) equivalent column.	|
|	$table->timestamps();	|	Adds nullable created_at and updated_atTIMESTAMP equivalent columns.	|
|	$table->timestampsTz();	|	Adds nullable created_at and updated_atTIMESTAMP (with timezone) equivalent columns.	|
|	$table->tinyIncrements('id');	|	Auto-incrementing UNSIGNED TINYINT (primary key) equivalent column.	|
|	$table->tinyInteger('votes');	|	TINYINT equivalent column.	|
|	$table->unsignedBigInteger('votes');	|	UNSIGNED BIGINT equivalent column.	|
|	$table->unsignedDecimal('amount', 8, 2);	|	UNSIGNED DECIMAL equivalent column with a precision (total digits) and scale (decimal digits).	|
|	$table->unsignedInteger('votes');	|	UNSIGNED INTEGER equivalent column.	|
|	$table->unsignedMediumInteger('votes');	|	UNSIGNED MEDIUMINT equivalent column.	|
|	$table->unsignedSmallInteger('votes');	|	UNSIGNED SMALLINT equivalent column.	|
|	$table->unsignedTinyInteger('votes');	|	UNSIGNED TINYINT equivalent column.	|
|	$table->uuid('id');	|	UUID equivalent column.	|
|	$table->year('birth_year');	|	YEAR equivalent column.	|
