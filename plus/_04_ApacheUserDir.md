# Apache UserDir
> _Emrullah İLÇİN_

Kullanıcılara Apache'yi kullanma yetkisinin verilmesi.

## Kullanıcıların public_html dizininin aktif hale getirilmesi (enable userdir)

* `sudo a2enmod userdir` # Kullanıcıların $HOME/public_html dizinine Apache Web sunucusu tanımlanır.
* `sudo mkdir $HOME/public_html`  # dizin yaratılır
* `sudo chown $USER.www-data $HOME/public_html`  # ile sahiplik verilir
* `sudo chmod 755 $USER.www-data $HOME/public_html`  # ile yetki verilir

* `/etc/apache2/mods-enabled/php7.2.conf` ta değişiklik yap kullanıcılar kendi dizininde php'yi kullanabilsin.

```
<IfModule mod_userdir.c>
    <Directory /home/*/public_html>
      php_admin_value engine On
    </Directory>
</IfModule>
```

* `sudo service apache2 restart`  # servis restart edilir.

> Debian'da `php_admin_value engine Off` durumunda bunu `On` yapıyoruz ki kullanıcılar public_html nin alt dizinlerini de kullansın. `.htaccess` tanımları yapsın.

## Test 

* `echo "<?php echo phpinfo() ?>" > $HOME/public_html/phpinfo.php`  # phpinfo.php'nin yaratılması
* Tarayıcı'da test ör: emr kullanıcısı için: `http://ilcin.name.tr/~emr/phpinfo.php`

:bulb: ~ (tilde) işareti Unix/Linux lerde home anlamına gelir. kullanıcıların html sayfalarını konulduğu dizini eşaret eder. ör: `/home/emr/public_html` gibi.
