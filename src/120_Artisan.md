# Artisan commands

php artisan is the default command line interface in laravel. 
It consists of number of commands which helps in developing a laravel application easily <small>
(_Laravel'in konsol komutudur.  Artisan komutu ile Model, Controller, Veritabanı ve Tablo ilişkileri gibi birçok işlemlerin yaratılması, değiştirilmesi, çalıştırılması gibi kullanımı vardır, detaylarına help ile bakılır._) </small>

> _Emrullah İLÇİN_

## Artisan "help" command

> Go to laravel project; ex:`cd $HOME/proje1`

```
php artisan                     # see help
php artisan help                # see help detail
php artisan help list           # see help list
php artisan help make:migration # see make migration 
php artisan --version        // see version on laravel 
```

> `php artisan down`  # `maintenance mode` mode with Apache; bakım modu<br>
> `php artisan up` # exit 'maintenance mode' and run http://localhost:8000 in browser<br>
> `php artisan serve --host=1.2.3.4 --port=8000` run "http://1.2.3.4:8000" in browser; ex 1.2.3.4 host, 800 is port; for exit ctrl-c <br>

## Artisan "migrate" command

laravel tables create in .`/database/migrations` directory and in the following pattern; `<date number>XXXX.TableName.php` 

```
php artisan migrate:rollback          # created table or tables are rollback
php artisan migrate:rollback --step=5 # rollback the last 5 records
php artisan migrate:refresh           # rollback and migrate are made for all tables
php artisan migrate:reset             # reset all tables
php artisan migrate:fresh             # refresh all tables
```

## Artisan "make:model" command

ex: `php artisan make:model model_name`

:bulb: php artisan make:model Tasks -mc # The model creates the transition and controller together.


## Artisan make:controller command 

:bulb: `php artisan make:controller TaskController`   # created is TaskController in "app/Http/Controllers/TasksController.php"

:bulb: `php artisan make:controller TaskController --resource --model=Tasks` # for "Create", "Read", "Update", "Delete" (CRUD)

## Artisan "tinker" commands

The "Tinker" command can view the model data, change it, etc. The "Tinker" prompt is ">>>". In the "Tinker" prompt "?" tinker commands come up when given or typing help. (some commands, history, exit)

<i>With `php artisan tinker`, records are read, record is entered, deleted, or random data is generated and table fields are filled. We enter data into the name field in the table we created with the operations below. In the example A name has been entered, you can enter a few more data. When you say save (), the record will be created in the SQL database.</i>

ex: If the model name is "Tasks"

run "php artisan tinker" while in the project directory.

```
>>> App\Tasks::first();  # If no Record is entered into the database, the output will be blank, otherwise the first record will come
>>> exit  # exit
```

> other single search;  ex: `echo "App\User::find(1);" | php artisan tinker`

* **Entering records into the database with the "artisan" command**
 
```
>>> $gorev =new App\Tasks;`    # Example: Tasks model. Let's assign this to "$gorev" class. We can use another name instead of "$gorev"
=> App\Tasks {#831}            # returning from tinker
>>> $gorev->name ="Emrullah";  # Assign "Emrullah" to name in $gorev class
=> "Emrullah"                  # returning from tinker
>>> $gorev->save();            # db e yaz.
=> true                        # returning from tinker
>>> App\Tasks::find(1);        # see first record  

App\Tasks {#849
     id: 1,
     name: Emrullah,
     created_at: null,
     updated_at: null,
   }
>>> exit  // exit
```

* **Artisan Tinker sql select commands** 

```
>>> App\Tasks::all()                          # see full recorts 
>>> App\Tasks::where('id', '>', 1)->get();    # if id > 1; sql commands in laravel 
>>> App\Tasks::where('id', '>=’, 1)->get();   # if id >=1; 
>>> App\Tasks::pluck('name');                 # see name is table
>>> App\Tasks::pluck('name')->first();        # see first recort

>>> DB::table('users')->insert(['name'=>'Erman','created_at'=>new DateTime,'updated_at'=>new DateTime])   # insert records in the table
```

## Artisan other commands

<i>clear commands</i><br>
`php artisan cache:clear`  # clear cache <br>
`php artisan config:clear` or  `rm -fr bootstrap/cache/config.php` # clear config ; For your safety, delete it when the project is finished<br>
`php artisan config:cache` # new config.php and bootstrap/cache/config.php; <br>
`php artisan view:clear`   # clear blade view cache <br>
`php artisan route:clear`  # clear route cache<br>
`php artisan event:clear`  #Events and listeners cache

<i>other><i>

­­­­­­```
php artisan migrate:status   # Database statis
php artisan route:list       # see routes

```

